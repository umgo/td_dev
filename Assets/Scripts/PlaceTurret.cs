﻿using UnityEngine;
using System.Collections;

public class PlaceTurret : MonoBehaviour {
	//Classın amacı kule yerleştirilebilen alanlar tıklandığında o alanda bir kule oluşmasını sağlar
    public GameObject turretPrefab;		//turret yapısı atanması
	private GameObject turret;			//turret nesnesi atanması

	//Fare tıklandığında çalışacak fonksiyon
	void OnMouseUp () {
		//Tıklanan yerde turret olup olmadığı kontrolü yapılır
		if (CanPlaceTurret())
		{
			//Eğer yoksa o noktada ve varsayılan açı ile bir turret nesnesi oluşturulur
			turret = Instantiate(turretPrefab, transform.position, Quaternion.identity) as GameObject;
			turret.transform.SetParent (GameObject.Find("Towers").transform);
		}
		
	}

	//Tıklanan noktada turret kontrolü
    bool CanPlaceTurret ()
    {
		//turret yoksa true döner
        return turret == null;
    }
}
