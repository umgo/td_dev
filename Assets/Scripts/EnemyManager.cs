﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EnemyManager : MonoBehaviour {
    private List<GameObject> _path;		//Düşman dönüş noktaları listesi
    private int _pathIndex;				//Kaçıncı dönüş noktası olduğunun sayacı
    private Transform _target;			//Düşmanın gideceği anlık dönüş noktası
	private int _health;
	//    private PlayerManager _player;
	public int Health
	{
		get { return _health; }
		set { _health = value; }
	}



    public float _speed;				//Düşmanın hareket hızı

	void Start () {
		//Sahnedeki Path etiketindeki dönüş noktalarını listeye atar atmadan önce isme göre küçükten büyüğe sıralar
        _path = GameObject.FindGameObjectsWithTag("Path").OrderBy(a => a.name).ToList();
		//Dönüş noktası sayacı
        _pathIndex = 0;
		//İlk dönüş noktası ayarlandı
        _target = _path[_pathIndex].transform;
//        _player = GameObject.Find("GM").GetComponent<PlayerManager>();
		_health=10;
	}

	void Update () {
        Move();
		if (isDeath())
			Destroy (gameObject);
	}

    void Move ()
    {
		//Düşmanın sürekli olarak dönüş noktasına doğru yönünün aynı olması sağlandı
        Vector3 dir = _target.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

		//Düşmanın yol boyunca ilerlemesi sağlandı
        transform.position = Vector3.MoveTowards(transform.position, _target.position, _speed * Time.deltaTime);
        _target = _path[_pathIndex].transform;

		//Dönüş noktasına geldiğinde dönüş noktası sayacı arttırıldı
        if (transform.position == _target.position && _pathIndex < _path.Count - 1)
            _pathIndex++;
    }

	public bool isDeath()
	{
		if (Health <= 0 || !gameObject.activeSelf)
			return true;
		else
			return false;
	}


}